/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "Robot.h"

#include "frc/commands/Scheduler.h"

std::shared_ptr<OI> Robot::oi;
std::shared_ptr<DriveSubsystem> Robot::drive_subsystem;
std::shared_ptr<DriveCommand> Robot::drive_command;
std::shared_ptr<BallSubsystem> Robot::ball_subsystem;
std::shared_ptr<BallCommand> Robot::ball_command;
std::shared_ptr<HatchSubsystem> Robot::hatch_subsystem;
std::shared_ptr<HatchCommand> Robot::hatch_command;
std::shared_ptr<ClimberSubsystem> Robot::climber_subsystem;
std::shared_ptr<ClimberCommand> Robot::climber_command;

void Robot::RobotInit() {
    oi = std::make_shared<OI>();

    drive_subsystem = std::make_shared<DriveSubsystem>();
    drive_command = std::make_shared<DriveCommand>(drive_subsystem, oi);

    ball_subsystem = std::make_shared<BallSubsystem>();
    ball_command = std::make_shared<BallCommand>(ball_subsystem, oi);

    hatch_subsystem = std::make_shared<HatchSubsystem>();
    hatch_command = std::make_shared<HatchCommand>(hatch_subsystem, oi);

    climber_subsystem = std::make_shared<ClimberSubsystem>();
    climber_command = std::make_shared<ClimberCommand>(climber_subsystem, oi);

    led = std::make_unique<frc::Spark>(0);

    OI::StartCameras(oi);
}

void Robot::RobotPeriodic() {
    if (ball_subsystem->HasBall())
        led->Set(-0.09);
    else if (hatch_subsystem->HasHatch())
        led->Set(0.87);
    else
        led->Set(-0.39);    
}

void Robot::DisabledInit() {}

void Robot::DisabledPeriodic() { frc::Scheduler::GetInstance()->Run(); }

void Robot::AutonomousInit() {
    drive_command->Start();
    ball_command->Start();
    hatch_command->Start();
    climber_command->Start();
}

void Robot::AutonomousPeriodic() { frc::Scheduler::GetInstance()->Run(); }

void Robot::TeleopInit() {
    drive_command->Start();
    ball_command->Start();
    hatch_command->Start();
    climber_command->Start();
}

void Robot::TeleopPeriodic() { frc::Scheduler::GetInstance()->Run(); }

void Robot::TestPeriodic() {}

#ifndef RUNNING_FRC_TESTS
int main() { return frc::StartRobot<Robot>(); }
#endif
