#include "commands/DriveCommand.h"

DriveCommand::DriveCommand(
    std::shared_ptr<DriveSubsystem> robot,
    std::shared_ptr<OI> interface) :
    drive(robot),
    oi(interface) {
    Requires(drive.get());

    halved = false;
}

void DriveCommand::Initialize() {}

void DriveCommand::Execute() {
    double left_joy = oi->xbox.GetY(frc::XboxController::JoystickHand::kLeftHand);
    double right_joy;

    if (frc::DriverStation::GetInstance().IsAutonomous())
        right_joy = -oi->xbox.GetX(frc::XboxController::JoystickHand::kRightHand);
    else
        right_joy = oi->xbox.GetY(frc::XboxController::JoystickHand::kRightHand);

    if (oi->xbox.GetBButton()) {
        left_joy /= 2;
        right_joy /= 2;
    }

    drive->SetDrive(left_joy, right_joy);
    drive->SetDashboard();
}

bool DriveCommand::IsFinished() { return false; }

void DriveCommand::End() {}

void DriveCommand::Interrupted() {}
