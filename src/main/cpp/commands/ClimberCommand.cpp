#include "commands/ClimberCommand.h"

ClimberCommand::ClimberCommand(
    std::shared_ptr<ClimberSubsystem> robot,
    std::shared_ptr<OI> interface):
    frc::Command("Climber Command"),
    climber_device(robot),
    oi(interface) {
    Requires(climber_device.get());
}

void ClimberCommand::Initialize() {}

void ClimberCommand::Execute() {
    if (oi->joy.GetRawButtonPressed(9))
        climber_device->SetFront(ClimberSubsystem::ClimberAction::lower);
    else if (oi->joy.GetRawButtonPressed(10))
        climber_device->SetFront(ClimberSubsystem::ClimberAction::raise);
    else if (oi->joy.GetRawButtonPressed(7))
        climber_device->SetBack(ClimberSubsystem::ClimberAction::raise);
    else if (oi->joy.GetRawButtonPressed(8))
        climber_device->SetBack(ClimberSubsystem::ClimberAction::lower);

    climber_device->SetDashboard();
}

bool ClimberCommand::IsFinished() { return false; }

void ClimberCommand::End() {}

void ClimberCommand::Interrupted() {}
