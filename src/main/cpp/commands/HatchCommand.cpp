#include "commands/HatchCommand.h"

HatchCommand::HatchCommand(
    std::shared_ptr<HatchSubsystem> robot,
    std::shared_ptr<OI> interface) :
    frc::Command("Hatch Command"),
    hatch_device(robot),
    oi(interface) {}

void HatchCommand::Initialize() {}

void HatchCommand::Execute() {
    if (oi->xbox.GetXButton()){
        if (!hatch_device->HasHatch())
            hatch_device->MoveHatch(HatchSubsystem::HatchAction::in);
        else
            hatch_device->MoveHatch(HatchSubsystem::HatchAction::stop);
    }
    else if (oi->joy.GetRawButton(5) || oi->xbox.GetYButton())
        hatch_device->MoveHatch(HatchSubsystem::HatchAction::out);
    else
        hatch_device->MoveHatch(HatchSubsystem::HatchAction::stop);

    if (oi->joy.GetRawButtonPressed(1))
        hatch_device->ToggleInOut();
}

bool HatchCommand::IsFinished() { return false; }

void HatchCommand::End() {}

void HatchCommand::Interrupted() {}
