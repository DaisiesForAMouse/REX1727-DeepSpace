#include "commands/BallCommand.h"

BallCommand::BallCommand(
    std::shared_ptr<BallSubsystem> robot,
    std::shared_ptr<OI> interface) :
    ball_device(robot),
    oi(interface) {
    Requires(ball_device.get());
    manual = false;
}

void BallCommand::Initialize() {
    ball_device->SetArm(BallSubsystem::ArmPos::top);
}

void BallCommand::Execute() {
    constexpr auto left = frc::XboxController::kLeftHand;
    constexpr auto right = frc::XboxController::kRightHand;

    constexpr auto up_side = right;
    constexpr auto down_side = left;

    if (oi->joy.GetRawButtonPressed(4)) {
        manual = !manual;
        if (!manual)
            ball_device->SetArm(BallSubsystem::ArmPos::top);
    }

    if (manual) {
        if (oi->xbox.GetBumper(up_side))
            ball_device->MoveArm(BallSubsystem::ArmAction::raise);
        else if (oi->xbox.GetBumper(down_side))
            ball_device->MoveArm(BallSubsystem::ArmAction::lower);
        else
            ball_device->MoveArm(BallSubsystem::ArmAction::stop);
    } else {
        if (oi->xbox.GetBumperPressed(up_side))
            ball_device->NextState(true);
        else if (oi->xbox.GetBumperPressed(down_side))
            ball_device->NextState(false);
        ball_device->SetComputed();
    }

    if (oi->xbox.GetTriggerAxis(right) > 0.35) {
        ball_device->SetIntake(BallSubsystem::IntakeAction::out);
    } else if (oi->xbox.GetTriggerAxis(left) > 0.35) {
        if (!ball_device->HasBall())
            ball_device->SetIntake(BallSubsystem::IntakeAction::in);
        else
            ball_device->SetIntake(BallSubsystem::IntakeAction::halt);
    } else {
        ball_device->SetIntake(BallSubsystem::IntakeAction::halt);
    }

    ball_device->SetDashboard();
}

bool BallCommand::IsFinished() { return false; }

void BallCommand::End() {}

void BallCommand::Interrupted() {}
