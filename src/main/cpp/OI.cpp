/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#include "OI.h"

OI::OI() : xbox(ports::xbox_controller), joy(ports::joystick) {}

void OI::CameraThread(std::shared_ptr<OI> oi) {
    auto server = frc::CameraServer::GetInstance();

    constexpr int width = 320;
    constexpr int height = 240;

    auto front_cam = server->StartAutomaticCapture(ports::front_cam);
    front_cam.SetResolution(width, height);
    front_cam.SetFPS(10);

    // auto back_cam = server->StartAutomaticCapture(ports::back_cam);
    // back_cam.SetResolution(width, height);
    // back_cam.SetFPS(10);

    auto front_sink = server->GetVideo(front_cam);
    // auto back_sink = server->GetVideo(back_cam);

    auto output_stream = server->PutVideo("Camera", width, height);

    cv::Mat source;
    cv::Mat output;

    bool front = true;
    std::uint64_t frame_time;

    cv::Point2f middle(width / 2, height / 2);
    auto rotate = cv::getRotationMatrix2D(middle, 110, 1);
    cv::Size output_size(width, height);

    while (true) {
        // if (oi->xbox.GetAButtonPressed()) {
        //     front = !front;

            // if (front) {
                // front_sink.SetEnabled(true);
                // back_sink.SetEnabled(false);
            // } else {
                // back_sink.SetEnabled(true);
                // front_sink.SetEnabled(false);
        //     }
        // }

        if (front)
            frame_time = front_sink.GrabFrame(source);
        // else
        //     frame_time = back_sink.GrabFrame(source);

        if (frame_time) {
            if (front)
                cv::warpAffine(source, output, rotate, output_size);
            else
                output = source;
            output_stream.PutFrame(output);
        }
    }
}

void OI::StartCameras(std::shared_ptr<OI> oi) {
    std::thread cam_thread(CameraThread, oi);
    cam_thread.detach();
}
