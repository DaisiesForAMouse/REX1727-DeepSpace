#include "subsystems/DriveSubsystem.h"

DriveSubsystem::DriveSubsystem() :
    frc::Subsystem("Drive Subsystem"),
    left_enc(ports::left_drive_enc_a, ports::left_drive_enc_b),
    right_enc(ports::right_drive_enc_a, ports::right_drive_enc_b),
    left_pid(left_p, left_i, left_d, left_f, left_enc, left_drive),
    right_pid(right_p, right_i, right_d, right_f, right_enc, right_drive),
    left_back(ports::left_back_drive),
    left_front(ports::left_front_drive),
    right_back(ports::right_back_drive),
    right_front(ports::right_front_drive),
    left_drive(left_back, left_front),
    right_drive(right_back, right_front),
    drive(left_drive, right_drive) {

    left_enc.SetDistancePerPulse(6 * 3.141 / 360);
    left_enc.SetReverseDirection(true);
    right_enc.SetDistancePerPulse(6 * 3.141 / 360);

    right_drive.SetInverted(true);
    left_drive.SetInverted(true);

    left_pid.SetPIDSourceType(frc::PIDSourceType::kRate);
    left_pid.SetOutputRange(-1.0, 1.0);
    right_pid.SetPIDSourceType(frc::PIDSourceType::kRate);
    right_pid.SetOutputRange(-1.0, 1.0);

    left_pid.SetPercentTolerance(5.0);
    right_pid.SetPercentTolerance(5.0);

    left_pid.SetSetpoint(0);
    right_pid.SetSetpoint(0);

    // left_pid.Enable();
    // right_pid.Enable();
    left_pid.Disable();
    right_pid.Disable();
}

void DriveSubsystem::SetLeftDrive(double spd, bool sqr) {
    if (std::abs(spd) > 0.05) {
        if (sqr)
            spd *= spd < 0 ? -spd : spd;
        // left_pid.SetSetpoint(max_speed * -spd);
        left_drive.Set(spd);
    } else {
        // left_pid.SetSetpoint(0);
        left_drive.StopMotor();
    }
}

void DriveSubsystem::SetRightDrive(double spd, bool sqr) {
    if (std::abs(spd) > 0.05) {
        if (sqr)
            spd *= spd < 0 ? -spd : spd;
        // right_pid.SetSetpoint(max_speed * -spd);
        right_drive.Set(spd);
    } else {
        // right_pid.SetSetpoint(0);
        right_drive.StopMotor();
    }
}

void DriveSubsystem::SetDrive(double l_spd, double r_spd, bool sqr) {
    if (frc::DriverStation::GetInstance().IsAutonomous()
        && (std::abs(l_spd) > 0.05 || std::abs(r_spd) > 0.05))
        drive.ArcadeDrive(l_spd * 0.85, r_spd * 0.55);
    else if (!frc::DriverStation::GetInstance().IsAutonomous()
             && (std::abs(l_spd) > 0.05 || std::abs(r_spd) > 0.05))
        drive.TankDrive(l_spd, r_spd);
    else
        drive.StopMotor();
    // SetLeftDrive(l_spd, sqr);
    // SetRightDrive(r_spd, sqr);
}

void DriveSubsystem::InitDefaultCommand() {}

void DriveSubsystem::SetDashboard() {
    frc::SmartDashboard::PutData(&left_pid);
    frc::SmartDashboard::PutData(&right_pid);
    frc::SmartDashboard::PutData(&left_enc);
    frc::SmartDashboard::PutData(&right_enc);
    frc::SmartDashboard::PutNumber("Left output", left_pid.Get());
    frc::SmartDashboard::PutNumber("Right output", right_pid.Get());
}
