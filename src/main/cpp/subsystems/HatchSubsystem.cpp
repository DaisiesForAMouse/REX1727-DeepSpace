#include "subsystems/HatchSubsystem.h"

HatchSubsystem::HatchSubsystem() :
    frc::Subsystem("Hatch Subsystem"),
    hatch_motor(ports::hatch),
    hatch_solenoid(ports::hatch_solenoid_a, ports::hatch_solenoid_b),
    hatch_switch(ports::hatch_switch) {}

void HatchSubsystem::InitDefaultCommand() {}

void HatchSubsystem::MoveHatch(HatchAction action) {
    switch (action) {
    case in:
        if (!HasHatch())
            hatch_motor.Set(0.8);
        break;
    case out:
        hatch_motor.Set(-0.8);
        break;
    case stop:
        hatch_motor.StopMotor();
        break;
    }
}

void HatchSubsystem::ToggleInOut() {
    if (hatch_solenoid.Get() == frc::DoubleSolenoid::kReverse)
        hatch_solenoid.Set(frc::DoubleSolenoid::kForward);
    else
        hatch_solenoid.Set(frc::DoubleSolenoid::kReverse);
}

bool HatchSubsystem::HasHatch() {
    return hatch_switch.Get();
}
