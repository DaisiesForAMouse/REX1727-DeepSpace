#include "subsystems/ClimberSubsystem.h"

ClimberSubsystem::ClimberSubsystem():
    frc::Subsystem("Climber Subsystem"),
    front_solenoid(ports::front_solenoid_a, ports::front_solenoid_b),
    back_solenoid(ports::back_solenoid_a, ports::back_solenoid_b) {}


void ClimberSubsystem::InitDefaultCommand() {}

void ClimberSubsystem::SetFront(ClimberAction action) {
    switch (action) {
    case raise:
        front_solenoid.Set(frc::DoubleSolenoid::kReverse);
        break;
    case lower:
        front_solenoid.Set(frc::DoubleSolenoid::kForward);
        break;
    }
}

void ClimberSubsystem::SetBack(ClimberAction action) {
    switch (action) {
    case raise:
        back_solenoid.Set(frc::DoubleSolenoid::kReverse);
        break;
    case lower:
        back_solenoid.Set(frc::DoubleSolenoid::kForward);
        break;
    }
}

void ClimberSubsystem::SetDashboard() {
    frc::SmartDashboard::PutBoolean("Front Piston Out", front_solenoid.Get() == frc::DoubleSolenoid::kForward);
    frc::SmartDashboard::PutBoolean("Back Piston Out", back_solenoid.Get() != frc::DoubleSolenoid::kForward);
}
