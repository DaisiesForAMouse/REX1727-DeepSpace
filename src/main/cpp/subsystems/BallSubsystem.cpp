#include "subsystems/BallSubsystem.h"

BallSubsystem::BallSubsystem():
    frc::Subsystem("Ball Subsystem"),
    arm_pid(arm_p, arm_i, arm_d, arm_f, arm_pot, dummy),
    intake(ports::intake),
    arm(ports::arm),
    dummy(-1),
    intake_switch_left(ports::intake_switch_left),
    intake_switch_right(ports::intake_switch_right),
    arm_pot(ports::arm_pot, 1932, -923) {
    arm.SetInverted(true);
    arm_pid.SetOutputRange(-1.0, 1.0);
    arm_pid.SetContinuous(false);
    arm_pid.SetInputRange(0, top_reading);
    arm_pid.Enable();
    arm_pid.SetPercentTolerance(1);
    arm_pid.SetSetpoint(0);

    state = top;
}

void BallSubsystem::InitDefaultCommand() {}

void BallSubsystem::MoveArm(BallSubsystem::ArmAction action) {
    switch (action) {
    case raise:
        arm.Set(-0.75);
        break;
    case lower:
        arm.Set(0.15);
        break;
    case stop:
        arm_pid.SetSetpoint(GetPot());
        SetComputed();
        break;
    }
}

void BallSubsystem::SetArm(BallSubsystem::ArmPos pos) {
    state = pos;
    switch (state) {
    case bottom:
        arm_pid.SetSetpoint(top_reading);
        break;
    case rocket:
        arm_pid.SetSetpoint(55);
        break;
    case cargo:
        arm_pid.SetSetpoint(35);
        break;
    case top:
        arm_pid.SetSetpoint(0);
        break;
    case hold:
        arm_pid.SetSetpoint(GetPot());
        break;
    }
}

double BallSubsystem::GetBaseline() {
    if (GetPot() < 30)
        return -0.3;
    else if (GetPot() < 60)
        return -0.4;
    else
        return -0.4;
}

void BallSubsystem::SetComputed() {
    double base = GetBaseline() * std::sin(GetPot() * 3.141 / 180.0);
    if (arm_pot.Get() > 87 && arm_pid.GetSetpoint() > 87)
        arm.StopMotor();
    else if (arm_pot.Get() < 2 && arm_pid.GetSetpoint() < 2)
        arm.StopMotor();
    else
        arm.Set(base + arm_pid.Get());
    std::cout << "REAL " << base + arm_pid.Get() << "\n";
}

void BallSubsystem::SetIntake(BallSubsystem::IntakeAction action) {
    switch (action) {
    case in:
        intake.Set(0.75);
        break;
    case out:
        intake.Set(-1.0);
        break;
    case halt:
        intake.StopMotor();
        break;
    }
}

void BallSubsystem::EnablePID(bool enable) {
    if (enable)
        arm_pid.Disable();
    else
        arm_pid.Enable();
}

void BallSubsystem::SetDashboard() {
    frc::SmartDashboard::PutNumber("Arm potentionmeter reading", arm_pot.Get());
    frc::SmartDashboard::PutData(&arm_pid);
    frc::SmartDashboard::PutNumber("P", arm_pid.GetP());
    frc::SmartDashboard::PutNumber("I", arm_pid.GetI());
    frc::SmartDashboard::PutNumber("D", arm_pid.GetD());
    frc::SmartDashboard::PutNumber("F", arm_pid.GetF());
    frc::SmartDashboard::PutNumber("Output", arm_pid.Get());
    frc::SmartDashboard::PutNumber("Setpoint", arm_pid.GetSetpoint());
    // std::cout << "P " << arm_pid.GetP() << "\n"
    //           << "I " << arm_pid.GetI() << "\n"
    //           << "D " << arm_pid.GetD() << "\n"
    //           << "F " << arm_pid.GetF() << "\n"
    //           << "Output " << arm_pid.Get() << "\n"
    //           << "Error " << arm_pid.GetError() << "\n";
}

bool BallSubsystem::HasBall() {
    return !intake_switch_left.Get() || !intake_switch_right.Get();
}

double BallSubsystem::GetPot() {
    return arm_pot.Get();
}

void BallSubsystem::NextState(bool up) {
    switch (state) {
    case bottom:
        state = up ? rocket : bottom;
        break;
    case rocket:
        state = up ? cargo : bottom;
        break;
    case cargo:
        state = up ? top : rocket;
        break;
    case top:
        state = up ? top : cargo;
        break;
    case hold:
        state = up ? top : bottom;
        break;
    }

    SetArm(state);
}
