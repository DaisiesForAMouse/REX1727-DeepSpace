/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#pragma once

#include <thread>
#include <memory>

#include "frc/XboxController.h"
#include "frc/Joystick.h"
#include "frc/smartdashboard/SmartDashboard.h"
#include "cscore_oo.h"
#include "opencv2/core/core.hpp"
#include "opencv2/core/types.hpp"
#include "opencv2/opencv.hpp"
#include "cameraserver/CameraServer.h"

#include "RobotMap.h"

class OI {
public:
    OI();

    frc::XboxController xbox;
    frc::Joystick joy;

    static void CameraThread(std::shared_ptr<OI> oi);
    static void StartCameras(std::shared_ptr<OI> oi);
};
