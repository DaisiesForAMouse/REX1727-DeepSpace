/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#pragma once

#include <memory>

#include "frc/TimedRobot.h"
#include "frc/Spark.h"
#include "cameraserver/CameraServer.h"

#include "OI.h"
#include "subsystems/DriveSubsystem.h"
#include "subsystems/BallSubsystem.h"
#include "subsystems/HatchSubsystem.h"
#include "subsystems/ClimberSubsystem.h"
#include "commands/DriveCommand.h"
#include "commands/BallCommand.h"
#include "commands/HatchCommand.h"
#include "commands/ClimberCommand.h"

class Robot : public frc::TimedRobot {
public:
    void RobotInit() override;
    void RobotPeriodic() override;
    void DisabledInit() override;
    void DisabledPeriodic() override;
    void AutonomousInit() override;
    void AutonomousPeriodic() override;
    void TeleopInit() override;
    void TeleopPeriodic() override;
    void TestPeriodic() override;
private:
    static std::shared_ptr<OI> oi;

    static std::shared_ptr<DriveSubsystem> drive_subsystem;
    static std::shared_ptr<BallSubsystem> ball_subsystem;
    static std::shared_ptr<HatchSubsystem> hatch_subsystem;
    static std::shared_ptr<ClimberSubsystem> climber_subsystem;

    static std::shared_ptr<DriveCommand> drive_command;
    static std::shared_ptr<BallCommand> ball_command;
    static std::shared_ptr<HatchCommand> hatch_command;
    static std::shared_ptr<ClimberCommand> climber_command;

    std::unique_ptr<frc::Spark> led;
};
