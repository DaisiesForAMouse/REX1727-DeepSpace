#pragma once

#include "frc/commands/Subsystem.h"
#include "frc/DoubleSolenoid.h"
#include "ctre/phoenix/motorcontrol/can/WPI_VictorSPX.h"
#include "frc/DigitalInput.h"

#include "RobotMap.h"

class HatchSubsystem: public frc::Subsystem {
public:
    enum HatchAction { in, out, stop,};
    HatchSubsystem();
    void InitDefaultCommand();
    void MoveHatch(HatchAction);
    void ToggleInOut();
    bool HasHatch();
private:
    ctre::phoenix::motorcontrol::can::WPI_VictorSPX hatch_motor;
    frc::DoubleSolenoid hatch_solenoid;
    frc::DigitalInput hatch_switch;
};
