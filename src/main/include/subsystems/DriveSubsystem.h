#pragma once

#include <memory>
#include <cmath>

#include "frc/commands/Subsystem.h"
#include "frc/SpeedControllerGroup.h"
#include "frc/drive/DifferentialDrive.h"
#include "frc/Encoder.h"
#include "frc/PIDController.h"
#include "frc/smartdashboard/SmartDashboard.h"
#include "frc/DriverStation.h"
#include "ctre/phoenix/motorcontrol/can/WPI_VictorSPX.h"

#include "RobotMap.h"

class DriveSubsystem: public frc::Subsystem {
public:
    DriveSubsystem();
    void InitDefaultCommand() override;
    void SetLeftDrive(double, bool sqr = true);
    void SetRightDrive(double, bool sqr = true);
    void SetDrive(double, double, bool sqr = true);
    void SetDashboard();
    frc::Encoder left_enc;
    frc::Encoder right_enc;
    frc::PIDController left_pid;
    frc::PIDController right_pid;
private:
    bool pid_enabled;
    ctre::phoenix::motorcontrol::can::WPI_VictorSPX left_back;
    ctre::phoenix::motorcontrol::can::WPI_VictorSPX left_front;
    ctre::phoenix::motorcontrol::can::WPI_VictorSPX right_back;
    ctre::phoenix::motorcontrol::can::WPI_VictorSPX right_front;

    frc::SpeedControllerGroup left_drive;
    frc::SpeedControllerGroup right_drive;

    frc::DifferentialDrive drive;

    static constexpr double left_p = 0.0005;
    static constexpr double left_i = 0.0000;
    static constexpr double left_d = 0.0005;
    static constexpr double left_f = 0.00725;

    static constexpr double right_p = 0.0005;
    static constexpr double right_i = 0.0000;
    static constexpr double right_d = 0.0005;
    static constexpr double right_f = 0.00725;

    static constexpr double max_speed = 150;
};
