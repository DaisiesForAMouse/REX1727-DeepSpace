#pragma once

#include "frc/commands/Subsystem.h"
#include "frc/DoubleSolenoid.h"
#include "frc/smartdashboard/SmartDashboard.h"

#include "RobotMap.h"

class ClimberSubsystem: public frc::Subsystem {
public:
    enum ClimberAction { raise, lower };
    ClimberSubsystem();
    void InitDefaultCommand() override;
    void SetFront(ClimberAction);
    void SetBack(ClimberAction);
    void SetDashboard();
private:
    frc::DoubleSolenoid front_solenoid;
    frc::DoubleSolenoid back_solenoid;
};
