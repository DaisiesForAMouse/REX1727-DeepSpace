#pragma once

#include <iostream>

#include "ctre/phoenix/motorcontrol/can/WPI_VictorSPX.h"
#include "ctre/phoenix/motorcontrol/can/WPI_TalonSRX.h"
#include "frc/commands/Subsystem.h"
#include "frc/DigitalInput.h"
#include "frc/PIDController.h"
#include "frc/AnalogPotentiometer.h"
#include "frc/smartdashboard/SmartDashboard.h"

#include "RobotMap.h"

class BallSubsystem: public frc::Subsystem {
public:
    enum ArmAction { raise, lower, stop };
    enum ArmPos { top, bottom, cargo, rocket, hold };
    enum IntakeAction { in, out, halt };
    BallSubsystem();
    void InitDefaultCommand() override;
    void MoveArm(ArmAction);
    void SetArm(ArmPos);
    void SetIntake(IntakeAction);
    void EnablePID(bool);
    double GetPot();
    void SetDashboard();
    bool HasBall();
    void SetComputed();
    void NextState(bool up = true);
    frc::PIDController arm_pid;
private:
    double GetBaseline();

    ArmPos state;

    ctre::phoenix::motorcontrol::can::WPI_VictorSPX intake;
    ctre::phoenix::motorcontrol::can::WPI_TalonSRX arm;
    ctre::phoenix::motorcontrol::can::WPI_TalonSRX dummy;

    frc::DigitalInput intake_switch_left;
    frc::DigitalInput intake_switch_right;

    frc::AnalogPotentiometer arm_pot;

    static constexpr double arm_p = 0.014;
    static constexpr double arm_i = 0.000;
    static constexpr double arm_d = 0.007;
    static constexpr double arm_f = 0.000;

    static constexpr double top_reading = 90;
};
