/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

#pragma once

namespace ports {
    constexpr int xbox_controller = 0;
    constexpr int joystick = 1;

    constexpr int left_back_drive = 1;
    constexpr int left_front_drive = 2;
    constexpr int right_back_drive = 3;
    constexpr int right_front_drive = 4;

    constexpr int left_drive_enc_a = 0;
    constexpr int left_drive_enc_b = 1;
    constexpr int right_drive_enc_a = 2;
    constexpr int right_drive_enc_b = 3;

    constexpr int front_cam = 0;
    constexpr int back_cam = 1;

    constexpr int arm = 5;
    constexpr int intake = 7;

    constexpr int arm_pot = 0;
    constexpr int intake_switch_left = 4;
    constexpr int intake_switch_right = 5;

    constexpr int hatch = 6;
    constexpr int hatch_solenoid_a = 4;
    constexpr int hatch_solenoid_b = 6;
    constexpr int hatch_switch = -1;

    constexpr int front_solenoid_a = 0;
    constexpr int front_solenoid_b = 5;
    constexpr int back_solenoid_a = 1;
    constexpr int back_solenoid_b = 7;
}
