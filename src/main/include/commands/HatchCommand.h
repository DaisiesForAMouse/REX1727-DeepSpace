#pragma once

#include <memory>

#include "frc/commands/Command.h"
#include "frc/XboxController.h"

#include "subsystems/HatchSubsystem.h"
#include "OI.h"

class HatchCommand: public frc::Command {
public:
    HatchCommand(std::shared_ptr<HatchSubsystem>, std::shared_ptr<OI>);
    void Initialize() override;
    void Execute() override;
    bool IsFinished() override;
    void End() override;
    void Interrupted() override;
private:
    std::shared_ptr<HatchSubsystem> hatch_device;
    std::shared_ptr<OI> oi;
};
