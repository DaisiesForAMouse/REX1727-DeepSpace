#pragma once

#include <memory>

#include "frc/commands/Command.h"
#include "frc/XboxController.h"

#include "subsystems/ClimberSubsystem.h"
#include "OI.h"

class ClimberCommand: public frc::Command {
public:
    ClimberCommand(std::shared_ptr<ClimberSubsystem>, std::shared_ptr<OI>);
    void Initialize() override;
    void Execute() override;
    bool IsFinished() override;
    void End() override;
    void Interrupted() override;
private:
    std::shared_ptr<ClimberSubsystem> climber_device;
    std::shared_ptr<OI> oi;
};
