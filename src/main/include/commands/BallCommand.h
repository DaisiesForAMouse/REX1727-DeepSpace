#pragma once

#include <iostream>
#include <cmath>

#include "frc/commands/Command.h"
#include "frc/XboxController.h"

#include "subsystems/BallSubsystem.h"
#include "OI.h"

class BallCommand: public frc::Command {
public:
    BallCommand(std::shared_ptr<BallSubsystem>, std::shared_ptr<OI>);
    void Initialize() override;
    void Execute() override;
    bool IsFinished() override;
    void End() override;
    void Interrupted() override;
private:
    bool manual;
    std::shared_ptr<BallSubsystem> ball_device;
    std::shared_ptr<OI> oi;
};
